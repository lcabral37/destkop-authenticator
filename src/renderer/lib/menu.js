import io from './io'
import store from '../store'

const { Menu } = require('electron').remote
let menu

export default {
  setupMenu: () => {
    menu = Menu.buildFromTemplate([
      {
        label: 'Authenticator',
        submenu: [
          {
            label: 'Open Database',
            click() {
              store.dispatch('password', undefined)
              io.open()
                .then(activateDatabase)
                .catch(errorHandler)
            }
          },
          {
            id: 'create',
            label: 'Create Database',
            click () {
              store.dispatch('password', undefined)
              io.save({})
                .then(activateDatabase)
                .catch(errorHandler)
            }
          },
          {
            id: 'save',
            label: 'Save Database',
            enabled: false,
            click () {
              io.save(store.state.database, store.state.database.path)
                .then(activateDatabase)
                .catch(errorHandler)
            }
          },
          {
            id: 'saveAs',
            label: 'Save Database as...',
            enabled: false,
            click () {
              io.save(store.state.database)
                .then(activateDatabase)
                .catch(errorHandler)
            }
          },
          {
            id: 'close',
            label: 'Close database',
            enabled: false,
            click() {
              if (!store.state.dirty) {
                console.log('close')
                store.dispatch('password', undefined)

                store.dispatch('database', {})
                disableMenuItem('save')
                disableMenuItem('close')
                disableMenuItem('saveAs')
                disableMenuItem('addEntry')
              }
            }
          },
          {
            id: 'addEntry',
            label: 'Add Entry',
            enabled: false,
            click() {
              console.log('add')
            }
          },
          {
            id: 'quit',
            label: 'Quit',
            click() {
              console.log('quit')
            }
          }
        ]
      }
    ])
    console.log(menu)
    Menu.setApplicationMenu(menu)
  },
  setAsActiveDatabase: () => {
    enableMenuItem('close')
    enableMenuItem('addEntry')
    enableMenuItem('save')
    enableMenuItem('saveAs')
  }
}

function activateDatabase(database) {
  console.log('dispatch', {database})
  store.dispatch('database', database)
  enableMenuItem('close')
  enableMenuItem('addEntry')
  enableMenuItem('save')
  enableMenuItem('saveAs')

  console.log('store', store.state)
}

function errorHandler(e) {
  console.log(e)
}

function enableMenuItem (id) {
  if (menu && menu.getMenuItemById(id)) {
    menu.getMenuItemById(id).enabled = true
  }
}

function disableMenuItem (id) {
  if (menu && menu.getMenuItemById(id)) {
    menu.getMenuItemById(id).enabled = false
  }
}
