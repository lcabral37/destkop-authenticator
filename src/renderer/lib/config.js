const fs = require('fs')

const homeDir = require('os').homedir()
const confFile = homeDir + '/.dAuth'
const defaultConfig = {
  recent: []
}

export default {
  load: () => {
    return new Promise(resolve => {
      fs.readFile(confFile, 'utf-8', (err, data) => {
        console.log({err, data})
        if (err) {
          return resolve(defaultConfig)
        }
        try {
          resolve(JSON.parse(data))
        } catch (e) {
          resolve(defaultConfig)
        }
      })
    })
  },
  save: (config) => {
    fs.writeFile(
      confFile,
      JSON.stringify(config, null, 2),
      'utf-8',
      () => {}
    )
  }
}
