import secureDatabase from './secureDb'
import store from '../store'

const { dialog } = require('electron').remote
const fs = require('fs')

const filters = [
  {
    name: 'E Autenticator databases',
    extensions: ['eauth']
  }
]

export default {
  open: async (file) => {
    if (!file) {
      file = await openDialog()
    }
    try {
      const database = toJson(fs.readFileSync(file, 'utf-8'))
      database.path = file
      database.entries = database.entries || []
      return Promise.resolve(database)
    } catch (err) {
      return Promise.reject(`Failed to open ${file}:` + err)
    }
  },

  save: async (database, toFile) => {
    if (store.state.password) {
      database = secureDatabase.encrypt(database, store.state.password)
    }
    if (!database) {
      database = {}
    }
    const file = toFile || await createFileDialog()

    try {
      const savedDatabase = {...database, file}
      fs.writeFileSync(file, fromJson(savedDatabase), 'utf-8')
      return Promise.resolve(savedDatabase)
    } catch (err) {
      return Promise.reject(`Failed to open ${file}:` + err)
    }
  }
}

function openDialog() {
  return new Promise((resolve, reject) => {
    dialog.showOpenDialog({
      filters,
      properties: ['openFile', 'showHiddenFiles']
    },
    filePaths => filePaths.length > 0 ? resolve(filePaths[0]) : reject('Dialog was canceled')
    )
  })
}

function createFileDialog() {
  return new Promise((resolve, reject) => {
    dialog.showSaveDialog({
      filters
    },
    file => file
      ? resolve(file.endsWith('.eauth') ? file : file + '.eauth')
      : reject('Dialog was canceled')
    )
  })
}

function toJson(data) {
  // data = window.atob(data)
  data = JSON.parse(data)
  return data
}

function fromJson(json) {
  json = JSON.stringify(json, null, 2)
  // json = window.btoa(json)
  return json
}
