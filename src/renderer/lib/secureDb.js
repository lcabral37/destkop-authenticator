import Cryptr from 'cryptr'

const ENCRYPT_SEP = '::'

export default {
  decrypt: (database, password) => {
    const cryptr = new Cryptr(password)
    const entries = database.entries.map(entry => {
      const [issuer, secret] = cryptr.decrypt(entry.secret).split(ENCRYPT_SEP)
      if (issuer === entry.issuer) {
        entry.secret = secret
        return entry
      }
      return undefined
    })
      .filter(entry => entry !== null)
    console.log({entries})
    if (entries.length === database.entries.length) {
      return {
        ...database,
        secured: false,
        entries
      }
    }
    return undefined
  },
  encrypt: (database, password) => {
    if (!password) {
      return {
        ...database,
        secured: false
      }
    }
    const cryptr = new Cryptr(password)
    const entries = database.entries.map(entry => {
      return {
        ...entry,
        secret: cryptr.encrypt([entry.issuer, entry.secret].join(ENCRYPT_SEP))
      }
    })

    return {
      ...database,
      secured: true,
      entries
    }
  }
}
