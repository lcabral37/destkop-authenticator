import Vue from 'vue'
import Vuex from 'vuex'
import Config from '../lib/config'
import SecureDatabase from '../lib/secureDb'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {

    dirty: true,
    password: undefined,
    database: {
      secured: false,
      path: undefined,
      entries: []
    },
    recent: []
  },
  mutations: {
    SET_DATABASE: (state, database) => {
      state.database = database
      state.dirty = false
      const recent = Array.concat([database.path], state.recent)
      state.recent = recent
        .filter((p, i, s) => s.indexOf(p) === i)
        .slice(0, 5)
      Config.save({recent: state.recent})
    },
    ADD_ENTRY: (state, entry) => {
      state.database.entries.push(entry)
      state.dirty = true
    },
    EDIT_ENTRY: (state, entry, index) => {
      state.database.entries[index](entry)
      state.dirty = true
    },
    REMOVE_ENTRY: (state, index) => {
      state.database.entries.splice(index, 1)
      state.dirty = false
    },
    SET_RECENT: (state, recent) => {
      state.recent = recent
    },
    SET_PASSWORD: (state, password) => {
      state.password = password
    }
  },
  actions: {
    database: ({ commit, state }, database) => {
      if (database.secured && state.password) {
        database = SecureDatabase.decrypt(database, state.password)
        if (!database) {
          return false
        }
      }
      commit('SET_DATABASE', database)
      return true
    },
    addEntry: ({ commit }, entry) => {
      commit('ADD_ENTRY', entry)
    },
    editEntry: ({ commit }, entry, index) => {
      commit('EDIT_ENTRY', entry, index)
    },
    removeEntry: ({ commit }, index) => {
      commit('REMOVE_ENTRY', index)
    },
    setRecent: ({ commit }, recent) => {
      commit('SET_RECENT', recent)
    },
    password: ({ commit, state }, password) => {
      if (password && state.database.secured) {
        console.log({password, state})
        const database = SecureDatabase.decrypt(state.database, password)
        if (!database) {
          return false
        }
        console.log({database})
        commit('SET_DATABASE', database)
      }
      commit('SET_PASSWORD', password)
      return true
    }
  }
})
