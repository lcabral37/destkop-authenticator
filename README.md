# Desktop Authenticator

> A Desktop Authenticator App
This is pretty much replicating the work th Google Authenticator does with some
 extra flavors such as:
 - storage in plain mode (probably not recomended)
 - storage in with password protection
 - multiple databases support (open and save to different files))
 - add/edit/delete each entry
 - generate & display time based token
 - QR code generation for easy export

### Why did I do this
The main purpose for this was not for a daily se but instead to be used as a backup storage to rebuilt my set ot time based key back into a new mobile
(similar to  https://gitlab.com/lcabral37/TerminalOathApp but this time with its own gui)


#### Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:9080
npm run dev

# build electron application for production
npm run build

# run unit & end-to-end tests
npm test


# lint all JS/Vue component files in `src/`
npm run lint

```
